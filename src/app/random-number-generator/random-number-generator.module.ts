import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RandomNumberGeneratorComponent } from './random-number-generator.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    RandomNumberGeneratorComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    RandomNumberGeneratorComponent
  ]
})
export class RandomNumberGeneratorModule { }
