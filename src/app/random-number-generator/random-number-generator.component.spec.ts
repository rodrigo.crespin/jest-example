import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RandomNumberGeneratorComponent } from './random-number-generator.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('RandomNumberGeneratorComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        ReactiveFormsModule
      ],
      declarations: [
        RandomNumberGeneratorComponent
      ],
    }).compileComponents();
  });

  it('should create component', () => {
    const fixture = TestBed.createComponent(RandomNumberGeneratorComponent);
    const comp = fixture.componentInstance;
    expect(comp).toBeTruthy();
  });

  it(`should start with model undefined`, () => {
    const fixture = TestBed.createComponent(RandomNumberGeneratorComponent);
    const comp = fixture.componentInstance;
    expect(comp.model).toEqual(undefined);
  });

  it(`should start with default min & max values`, () => {
    const fixture = TestBed.createComponent(RandomNumberGeneratorComponent);
    const comp = fixture.componentInstance;
    const form = comp.form.value;
    expect(form.min).not.toBeNull();
    expect(form.max).not.toBeNull();
  });

  it(`should get a random number between min and max`, () => {
    const fixture = TestBed.createComponent(RandomNumberGeneratorComponent);
    const comp = fixture.componentInstance;
    const min = 15;
    const max = 18;
    const r1 = comp.getRandomNumber(min, max);
    const r2 = comp.getRandomNumber(min, max);
    expect(r1).toBeGreaterThanOrEqual(min);
    expect(r1).toBeLessThanOrEqual(max);
    expect(r1).not.toEqual(r2);
  });

  it('should render generate button enabled', () => {
    const fixture = TestBed.createComponent(RandomNumberGeneratorComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('button')?.disabled).toEqual(false);
  });
});
