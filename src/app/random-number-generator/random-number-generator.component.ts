import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

const MIN_VALUE = 0;
const MAX_VALUE = 999;

interface RandomNumberModel {
  number: number;
}

@Component({
  selector: 'app-random-number-generator',
  templateUrl: './random-number-generator.component.html',
  styleUrls: ['./random-number-generator.component.scss']
})
export class RandomNumberGeneratorComponent {
  form: FormGroup;
  model?: RandomNumberModel;

  constructor(private fb: FormBuilder) {
    this.model = undefined;
    this.form = this.buildFormGroup(fb);
  }

  getRandomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  generate(): void {
    if (this.form.invalid) {
      return;
    }
    const form = this.form.value;
    const number = this.getRandomNumber(form.min || MIN_VALUE, form.max || MAX_VALUE);
    this.model = { number };
  }

  private buildFormGroup(fb: FormBuilder): FormGroup {
    const min = fb.control(MIN_VALUE, [Validators.required, Validators.min(MIN_VALUE), Validators.max(MAX_VALUE)]);
    const max = fb.control(MAX_VALUE, [Validators.required, Validators.min(MIN_VALUE), Validators.max(MAX_VALUE)]);
    return fb.group({ min, max });
  }
}
